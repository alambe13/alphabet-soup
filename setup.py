import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
setuptools.setup(
     name='alphabet-soup-lambert',
     version='0.1',
     scripts=['lambert_soup.py'] ,
     author="Angela Lambert",
     author_email="alambe13@terpmail.umd.edu",
     description="A word search project",
     long_description=long_description,
   long_description_content_type="text/markdown",
     url="https://github.com/alambe13/alphabet-soup",
     packages=setuptools.find_packages(),
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
     python_requires='>=2.7',
 )
