import unittest, pytest
import lambert_soup, test_files, answer_files


class Test(unittest.TestCase):
    def test_horizontal(self):
        lambert_soup.analyze('test_files/prac_horiz.txt')
        file_out = open('output_file.txt', 'r')
        file_ans = open('answer_files/horiz_ans.txt', 'r')
        assert file_out.read() == file_ans.read()

    def test_vertical(self):
        lambert_soup.analyze('test_files/prac_vert.txt')
        file_out = open('output_file.txt', 'r')
        file_ans = open('answer_files/vert_ans.txt', 'r')
        assert file_out.read() == file_ans.read()

    def test_diagonal(self):
        lambert_soup.analyze('test_files/prac_diag.txt')
        file_out = open('output_file.txt', 'r')
        file_ans = open('answer_files/diag_ans.txt', 'r')
        assert file_out.read() == file_ans.read()

    def test_all(self):
        lambert_soup.analyze('test_files/prac_all.txt')
        file_out = open('output_file.txt', 'r')
        file_ans = open('answer_files/all_ans.txt', 'r')
        assert file_out.read() == file_ans.read()
